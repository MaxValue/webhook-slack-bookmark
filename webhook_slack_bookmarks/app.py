#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, request
import requests as r
from os import environ
from ruamel.yaml import YAML


app = Flask(__name__)
config_file_path = environ.get("CONFIG_FILE")
yamlLoader=YAML(typ='safe')
app.config.from_file(config_file_path, load=yamlLoader.load)


def _get_slack_bookmarks(config):
    slack_channel = config.get("channel", "")
    res = r.post(
        f"https://slack.com/api/bookmarks.list",
        headers={
            "Authorization": f"Bearer {config['slack_token']}",
            "Content-type": "application/json; charset=utf-8",
        },
        json={
            "token": config["slack_token"],
            "channel_id": slack_channel,
        },
    )
    try:
        payload = res.json()
        app.logger.debug(f"Got payload bookmarks: {payload}")
        return payload["bookmarks"]
    except:
        return


def _find_slack_bookmark(config, bookmarks):
    for bookmark in bookmarks:
        matches = True
        for key, value in config.get("bookmark", {}).items():
            if key in bookmark and bookmark[key] != value:
                matches = False
        if matches:
            app.logger.debug(f"Found bookmark: {bookmark}")
            return bookmark


def _sanitize_bookmark_config(bookmark_config):
    emoji = bookmark_config.get("emoji", "")
    if not (emoji.startswith(":") or emoji.endswith(":")):
        bookmark_config["emoji"] = f":{emoji}:"
    return bookmark_config


def _update_slack_bookmark(config, bookmark, state_config):
    state_config = _sanitize_bookmark_config(state_config)
    res = r.post(
        f"https://slack.com/api/bookmarks.edit",
        headers={
            "Authorization": f"Bearer {config['slack_token']}",
            "Content-type": "application/json; charset=utf-8",
        },
        json={
            "token": config["slack_token"],
            "bookmark_id": bookmark["id"],
            "channel_id": bookmark["channel_id"],
            **state_config,
        },
    )
    try:
        app.logger.debug(f"Got response")
        payload = res.json()
        app.logger.debug(f"Payload: {payload}")
        return payload["bookmark"]
    except:
        return


def sync_slack_bookmarks(config, status):
    for bookmark_config in config["bookmarks"]:
        slack_channel = bookmark_config.get("bookmark", {}).get("channel", "")
        if status in bookmark_config.get("states").keys():
            state_config = bookmark_config["states"][status]
        else:
            app.logger.debug(f"No state config")
            return
        bookmarks = _get_slack_bookmarks(bookmark_config)
        if not bookmarks:
            app.logger.debug(f"No bookmarks")
            return
        bookmark = _find_slack_bookmark(bookmark_config, bookmarks)
        if not bookmark:
            app.logger.debug(f"No bookmark matched")
            return
        _update_slack_bookmark(bookmark_config, bookmark, state_config)


@app.route("/hook", methods=["POST"])
def hook_endpoint():
    app.logger.debug("Hook endpoint launched!")

    authorization_header = request.headers.get("Authorization")
    for hook in app.config["HOOKS"]:
        if authorization_header == hook.get("auth", ""):
            hook_config = hook
            break
    else:
        app.logger.debug(f"Unauthorized")
        return "Unauthorized", 401

    app.logger.debug(f"Authorized")

    try:
        data = request.get_json()
        if not data:
            return "Error", 500

    except Exception as e:
        return "Error", 500

    heartbeat = data.get("heartbeat", {})
    if not heartbeat:
        return "Got test", 200
    status = heartbeat.get("status", False)
    if type(status) != int:
        return "Got test", 200

    app.logger.debug(f"Status is {status}")

    sync_slack_bookmarks(hook_config, status)

    return "Success", 200


if __name__ == "__main__":
    app.run(port=3023)
