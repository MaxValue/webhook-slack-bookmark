# Webhook Server to Change Slack Bookmarks

A simple little Flask web server which changes a Slack channel bookmark according to a received webhook payload.

## Contents
[TOC]

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

* [Python 3](https://www.python.org/downloads/)
* pip to install the app

### Installing

Setup a virtualenv
```
python3 -m venv .venv
```

Install this app
```
pip install --upgrade webhook-slack-bookmarks
```

Install gunicorn or whatever web server you want to use
```
pip install --upgrade gunicorn
```

## Deployment

Create a systemd service unit: `webhook-slack-bookmarks.service`
```ini
[Unit]
Description=Webhook Server to update Slack bookmarks
After=network.target

[Service]
User=your service user here
Group=your service user group here
WorkingDirectory=/path/to/your/app/dir
Environment="PATH=.venv/bin"
ExecStart=.venv/bin/gunicorn webhook_slack_bookmarks.app:app -b 127.0.0.1:3023

[Install]
WantedBy=multi-user.target
```

Don’t forget to configure your web server (e.g. NGINX) to forward requests to this internal port.

## Built With

* [Sublime Text 3](https://www.sublimetext.com/) - The code editor I use

### Roadmap
Things I already plan to implement, but didn't have yet:
- [ ] Add tests
- [ ] Support other status pages than Uptime Kuma

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](-/tags).

## Authors

* **Max Fuxjäger** - *Initial work* - [MaxValue](https://gitlab.com/MaxValue)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

### Project History
This project was created because I (Max) wanted to have a simple user friendly status display of my services inside Slack.